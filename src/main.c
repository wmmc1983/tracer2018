/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/


#include "stm32f3xx.h"


/* 定数 */
#define INTERVAL 10000
#define DUTY_FAST 0.75F
#define DUTY_SLOW 0.5F

/* 機体が前進する際のモータの回転方向 */
#define MOTOR_R_FOWARD 2
#define MOTOR_L_FOWARD 1

/* 機体の前進・後退を表す変数 */
#define FOWARD 1
#define BACK 0

/* センサ閾値 */
#define THRESHOLD 0.5F

void port_init(void) {
	//
	RCC->AHBENR |= RCC_AHBENR_GPIOAEN;
	RCC->AHBENR |= RCC_AHBENR_GPIOBEN;

	//
	GPIOB->MODER &= ~GPIO_MODER_MODER0_Msk;
	GPIOB->MODER |= (GPIO_MODE_OUTPUT_PP << GPIO_MODER_MODER0_Pos);

	GPIOB->MODER &= ~GPIO_MODER_MODER1_Msk;
	GPIOB->MODER |= (GPIO_MODE_OUTPUT_PP << GPIO_MODER_MODER1_Pos);

	GPIOB->MODER &= ~GPIO_MODER_MODER6_Msk;
	GPIOB->MODER |= (GPIO_MODE_OUTPUT_PP << GPIO_MODER_MODER6_Pos);

	GPIOB->MODER &= ~GPIO_MODER_MODER7_Msk;
	GPIOB->MODER |= (GPIO_MODE_OUTPUT_PP << GPIO_MODER_MODER7_Pos);

	//
	GPIOA->MODER &= ~GPIO_MODER_MODER11_Msk;
	GPIOA->MODER |= (GPIO_MODE_INPUT << GPIO_MODER_MODER11_Pos);
	GPIOA->PUPDR &= ~GPIO_PUPDR_PUPDR11_Msk;
	GPIOA->PUPDR |= (GPIO_PULLUP << GPIO_PUPDR_PUPDR11_Pos);

	GPIOA->MODER &= ~GPIO_MODER_MODER12_Msk;
	GPIOA->MODER |= (GPIO_MODE_INPUT << GPIO_MODER_MODER12_Pos);
	GPIOA->PUPDR &= ~GPIO_PUPDR_PUPDR12_Msk;
	GPIOA->PUPDR |= (GPIO_PULLUP << GPIO_PUPDR_PUPDR12_Pos);

	GPIOA->MODER &= ~GPIO_MODER_MODER15_Msk;
	GPIOA->MODER |= (GPIO_MODE_INPUT << GPIO_MODER_MODER15_Pos);
	GPIOA->PUPDR &= ~GPIO_PUPDR_PUPDR15_Msk;
	GPIOA->PUPDR |= (GPIO_PULLUP << GPIO_PUPDR_PUPDR15_Pos);
}


void sysclk_init(void) {
	RCC->CFGR |= RCC_CFGR_PLLSRC_HSI_DIV2; // PLL <== HSI/2 = 4MHz
	RCC->CFGR |= RCC_CFGR_PPRE1_DIV2; // APB1 = PLL / 2 = 32MHz
	RCC->CFGR |= ( (16 - 1) << RCC_CFGR_PLLMUL_Pos); // x16
	FLASH->ACR |= FLASH_ACR_LATENCY_1; // flash access latency for 48 < HCLK <= 72. This statement must be placed immediately after PLL multiplication.
	RCC->CR |= RCC_CR_PLLON;
	while(!(RCC->CR & RCC_CR_PLLRDY)); // wait until PLL is ready
	RCC->CFGR |= RCC_CFGR_SW_PLL; // PLL as system clock

	while( (RCC->CFGR & RCC_CFGR_SWS_Msk) != RCC_CFGR_SWS_PLL );
	SystemCoreClockUpdate();
}


void ms_wait(uint32_t ms) {
	SysTick->LOAD = 8000 - 1; // sysclk = 64MHz, prescaled by 8
	SysTick->VAL = 0; // reset count value
	SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk; // count start

	for(uint32_t i = 0; i < ms; i++) {
		while( !(SysTick->CTRL & SysTick_CTRL_COUNTFLAG_Msk) ); // wait for 1ms count
	}
	SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk; // count stop
}


void timer_init(void) {

	// PA0 を Alternative Function に設定する
	GPIOA->MODER &= ~(1 << 0);
	GPIOA->MODER |= 1 << 1;
	// PA0 を AF1 に設定する
	GPIOA->AFR[0] |= 1 << 0;

	// PA1 を Alternative Function に設定する
	GPIOA->MODER &= ~(1 << 2);
	GPIOA->MODER |= 1 << 3;
	// PA1 を AF1 に設定する
	GPIOA->AFR[0] |= 1 << 4;


	RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;

	TIM2->CR1 = TIM_CR1_CEN;						//タイマ有効
	TIM2->CR2 = 0;
	TIM2->CCMR1 = TIM_CCMR1_OC1M_2 | TIM_CCMR1_OC1M_1 | TIM_CCMR1_OC1PE |
			TIM_CCMR1_OC2M_2 | TIM_CCMR1_OC2M_1 | TIM_CCMR1_OC2PE;	//PWMモード1
	TIM2->CCER = TIM_CCER_CC1E | TIM_CCER_CC2E;		//TIM2_CH1出力有効
	TIM2->BDTR = TIM_BDTR_MOE;

	TIM2->CNT = 0;					//タイマカウンタ値を0にリセット
	TIM2->PSC = 63;					//タイマのクロック周波数をシステムクロック/64=1MHzに設定
	TIM2->ARR = INTERVAL;	//タイマカウンタの上限値。取り敢えずDEFAULT_INTERVAL(params.h)に設定
	TIM2->CCR1 = 0;				//タイマカウンタの比較一致値
	TIM2->CCR2 = 0;				//タイマカウンタの比較一致値



	// PB4 を Alternative Function に設定する
	GPIOB->MODER &= ~(1 << 8);
	GPIOB->MODER |= 1 << 9;
	// PB4 を AF2 に設定する
	GPIOB->AFR[0] |= 1 << 17;

	// PB5 を Alternative Function に設定する
	GPIOB->MODER &= ~(1 << 10);
	GPIOB->MODER |= 1 << 11;
	// PB5 を AF2 に設定する
	GPIOB->AFR[0] |= 1 << 21;

	RCC->APB1ENR |= RCC_APB1ENR_TIM3EN;

	TIM3->CR1 = TIM_CR1_CEN;						//タイマ有効
	TIM3->CR2 = 0;
	TIM3->CCMR1 = TIM_CCMR1_OC1M_2 | TIM_CCMR1_OC1M_1 | TIM_CCMR1_OC1PE |
			TIM_CCMR1_OC2M_2 | TIM_CCMR1_OC2M_1 | TIM_CCMR1_OC2PE;	//PWMモード1
	TIM3->CCER = TIM_CCER_CC1E | TIM_CCER_CC2E;		//TIM2_CH1出力有効
	TIM3->BDTR = TIM_BDTR_MOE;

	TIM3->CNT = 0;					//タイマカウンタ値を0にリセット
	TIM3->PSC = 63;					//タイマのクロック周波数をシステムクロック/64=1MHzに設定
	TIM3->ARR = INTERVAL;	//タイマカウンタの上限値。取り敢えずDEFAULT_INTERVAL(params.h)に設定
	TIM3->CCR1 = 0;				//タイマカウンタの比較一致値
	TIM3->CCR2 = 0;				//タイマカウンタの比較一致値

}


void motor_R(uint32_t dir, float duty) {
	if((dir == FOWARD && MOTOR_R_FOWARD == 1) || (dir == BACK && MOTOR_R_FOWARD == 2)){
		TIM3->CCR1 = INTERVAL * duty;
		TIM3->CCR2 = 0;
	}else{
		TIM3->CCR1 = 0;
		TIM3->CCR2 = INTERVAL * duty;
	}
}


void motor_L(uint32_t dir, float duty) {
	if((dir == FOWARD && MOTOR_L_FOWARD == 1) || (dir == BACK && MOTOR_L_FOWARD == 2)){
		TIM2->CCR1 = INTERVAL * duty;
		TIM2->CCR2 = 0;
	}else{
		TIM2->CCR1 = 0;
		TIM2->CCR2 = INTERVAL * duty;
	}
}


void adc_init(void) {
	// PA4 を Analog モードに
	GPIOA->MODER |= 1 << 8;
	GPIOA->MODER |= 1 << 9;
	// PA5 を Analog モードに
	GPIOA->MODER |= 1 << 10;
	GPIOA->MODER |= 1 << 11;
	// PA6 を Analog モードに
	GPIOA->MODER |= 1 << 12;
	GPIOA->MODER |= 1 << 13;
	// PA7 を Analog モードに
	GPIOA->MODER |= 1 << 14;
	GPIOA->MODER |= 1 << 15;

	RCC->CFGR2 |= RCC_CFGR2_ADCPRE12_DIV1; // PLL 1x prescale supply
	RCC->AHBENR |= RCC_AHBENR_ADC12EN;

	ADC2->CR &= ~ADC_CR_ADVREGEN_Msk;
	ADC2->CR |= ADC_CR_ADVREGEN_0; // enable voltage regulator

	ADC2->SQR1 &= ~ADC_SQR1_L_Msk; // 1 channel
	ADC2->CR |= ADC_CR_ADEN; // ADC1 peripheral enabled
}


uint32_t adc_read(uint32_t ch) {
	ADC2->SQR1 &= ~ADC_SQR1_SQ1_Msk; // delete the channel information
	ADC2->SQR1 |= (ch << ADC_SQR1_SQ1_Pos); // channel number of ADC1
	ADC2->CR |= ADC_CR_ADSTART; // start a/d convert

	while( !(ADC2->ISR & ADC_ISR_EOC_Pos) ); // wait until ADC is done
	ADC2->ISR |= ADC_ISR_EOC;
	return ADC2->DR;
}


int main(void) {
	//
	sysclk_init();
	port_init();
	timer_init();
	adc_init();

	for(;;) {
		GPIOB->ODR &= ~GPIO_ODR_0;
		GPIOB->ODR &= ~GPIO_ODR_1;
		GPIOB->ODR &= ~GPIO_ODR_6;
		GPIOB->ODR &= ~GPIO_ODR_7;
		if( adc_read(4) < 4096 * THRESHOLD ) {
			GPIOB->ODR |= GPIO_ODR_0;
			motor_L(FOWARD, DUTY_FAST);
		}else if( adc_read(3) < 4096 * THRESHOLD ) {
			GPIOB->ODR |= GPIO_ODR_1;
			motor_L(FOWARD, DUTY_SLOW);
		}else{
			motor_L(FOWARD, 0);
		}
		if( adc_read(1) < 4096 * THRESHOLD ) {
			GPIOB->ODR |= GPIO_ODR_7;
			motor_R(FOWARD, DUTY_FAST);
		}else if( adc_read(2) < 4096 * THRESHOLD ) {
			GPIOB->ODR |= GPIO_ODR_6;
			motor_R(FOWARD, DUTY_SLOW);
		}else{
			motor_R(FOWARD, 0);
		}
	}
	return 0;
}
